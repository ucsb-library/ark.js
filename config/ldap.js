module.exports = {
  server: {
    searchFilter: process.env.LDAP_FILTER || "(userprincipalname={{username}})",
    searchBase: process.env.LDAP_BASE,
    url: process.env.LDAP_URL,
    bindCredentials: process.env.LDAP_PASS,
    bindDN: process.env.LDAP_USER
  }
}
