const basic = {
  from: process.env.SMTP_FROM,
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT || 25,
  secure: (process.env.SMTP_TLS ? true : false)
}

let auth = {}
if (process.env.SMTP_USER && process.env.SMTP_PASS) {
  auth = {
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASS
    }
  }
}

module.exports = Object.assign(auth, basic)
