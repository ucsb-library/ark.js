const assert = require('assert')
const parseSheet = require('../lib/parse_sheet.js')
const XLSX = require('xlsx')

const expected = [
  {
    '_target': '',
    'erc.what': 'english muffin',
    'erc.when': '1859',
    'erc.who': 'new yorkers'
  },
  {
    '_target': 'https://twitter.com',
    'erc.what': 'the worst website',
    'erc.when': '03/21/06',
    'erc.who': ''
  }
]


describe('parser', () => {
  it('should correctly parse a SheetJS object from an XLS', () => {
    const sheet = XLSX.readFile('test/fixtures/erc_0.xls').Sheets.Sheet1
    const parsed = parseSheet(sheet, 'erc')

    assert.deepEqual(parsed, expected)
  })

  it('should correctly parse a SheetJS object from an XLSX', () => {
    const sheet = XLSX.readFile('test/fixtures/erc_0.xlsx').Sheets.Sheet1
    const parsed = parseSheet(sheet, 'erc')

    assert.deepEqual(parsed, expected)
  })

  it('should correctly parse a SheetJS object with offset', () => {
    const sheet = XLSX.readFile('test/fixtures/erc_1_offset.xlsx').Sheets.Sheet1
    const parsed = parseSheet(sheet, 'erc')

    assert.deepEqual(
      parsed,
      [
        {
          '_target': '',
          'erc.what': 'english muffin',
          'erc.when': '1859',
          'erc.who': 'new yorkers'
        },
        {
          '_target': 'https://twitter.com',
          'erc.what': 'the worst website',
          'erc.when': '21/3/2006',
          'erc.who': ''
        }
      ]
    )
  })
})
