const assert = require('assert')
const inputFieldFn = require('../lib/render/tables/input_field_fn.js')

describe('inputFieldfn', () => {
  it('should return a working function when given valid parameters', () => {
    const fun = inputFieldFn('text', 'erc.who')
    assert.equal(
      fun('new', 'erc', 0, 'seamus '),
      "<td><input class='erc.who' type='text' name='new[erc][0][erc.who]' value='seamus' placeholder=''></td>"
    )
  })

  it('should return a function that works when given no cell value', () => {
    const fun = inputFieldFn('text', 'erc.who')
    assert.equal(
      fun('new', 'erc', 2),
      "<td><input class='erc.who' type='text' name='new[erc][2][erc.who]' value='' placeholder=''></td>"
    )
  })
})
