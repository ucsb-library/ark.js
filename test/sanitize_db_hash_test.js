const assert = require('assert')
const sanitize = require('../lib/hash_transforms/sanitize_db_hash.js')

const clean = {
  'erc_who': 'jan',
  'erc_when': 'jan',
  'array': [
    'heck%2C%20me%3F'
  ],
  'nonstandard_headers': {
    'expected%3F': 'no',
    'unexpected%20header': ''
  }
}

describe('parse.db.in', () => {
  it('should turn an EzID hash into a DB-compatible hash', () => {
    const dirty = {
      'erc.who': 'jan',
      'erc.when': 'jan',
      'array': ['heck, me?'],
      'nonstandard_headers': {
        'unexpected header': '',
        'expected?': 'no'
      }
    }

    assert.deepEqual(
      sanitize.in(dirty),
      clean
    )
  })
})

describe('parse.db.in', () => {
  it('should turn a hash from the DB into an human-readable hash', () => {
    const out = {
      'erc_who': 'jan',
      'erc_when': 'jan',
      'array': ['heck, me?'],
      'nonstandard_headers': {
        'unexpected header': '',
        'expected?': 'no'
      }
    }

    assert.deepEqual(
      sanitize.out(clean),
      out
    )
  })
})
