const assert = require('assert')
const isStandardHeader = require('../lib/is_standard_header.js')

describe('isStandardHeader', () => {
  it('should error when given an invalid profile', () => {
    assert.throws(
      () => {
        isStandardHeader('honk', 'dc.publisher')
      },
      /Invalid profile honk/
    )
  })

  it('should return false when no header is specified', () => {
    assert.equal(
      isStandardHeader('fake'),
      false
    )
  })

  it('should return false when nonstandard', () => {
    assert.equal(
      isStandardHeader('erc', 'hecko'),
      false
    )
  })

  it("should return true when there's extra whitespace", () => {
    assert.equal(
      isStandardHeader('datacite', 'resource type'),
      true
    )
  })
  it('should return true when standard', () => {
    assert(
      isStandardHeader('dc', 'dc.publisher')
    )
  })

  it('should return false when wrong profile', () => {
    assert.equal(
      isStandardHeader('erc', 'dc.publisher'),
      false
    )
  })
})
