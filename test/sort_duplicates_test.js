const assert = require('assert')
const duplicates = require('../lib/sort_duplicates.js')

const dupes = [
  {
    'erc.what': 'the best website',
    'erc.when': '03/21/06',
    'erc.who': '',
    '_target': 'https://twitter.com'
  },
  {
    'erc.what': 'the worst website',
    'erc.when': '03/21/06',
    'erc.who': '',
    '_target': 'https://twitter.com'
  }
]

describe('sortDuplicates', () => {
  it('should treat them as duplicates', () => {
    assert.deepEqual(
      duplicates([dupes[0]], [dupes[1]]),
      {
        create: [],
        update: [dupes[0]]
      }
    )
  })
})
