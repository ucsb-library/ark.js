const headers = require('./render/tables/headers.js')
const util = require('util')

/**
 * Parse the range string provided by SheetJS (e.g., 'C5:F7') and return a hash object with each
 * column and row included in the range, e.g.: { columns: [ 'C', 'D', 'E', 'F' ], rows: [ 5, 6, 7 ] }
 *
 * FIXME: the range from SheetJS includes empty columns and rows
 *
 * @param {String} range
 * @return {{ columns: Array<string>, rows: Array<number> }}
 */
function parseRange(range) {
  const [start, stop] = range.split(':')

  // https://stackoverflow.com/a/22624453
  const columnStart = start[0].charCodeAt(0) - 65
  const columnEnd = stop[0].charCodeAt(0) - 65

  const rowStart = parseInt(start[1])
  const rowEnd = parseInt(stop[1])

  const columnRange = [...Array(columnEnd - columnStart + 1).keys()].map(value => {
    return String.fromCharCode((value + columnStart) + 65)
  })

  const rowRange = [...Array(rowEnd - rowStart + 1).keys()].map(value => {
    return value + rowStart
  })

  return {
    columns: columnRange,
    rows: rowRange
  }
}

/**
 * Convert a header CELL to an EzID-compatible format according to the given PROFILE.
 * @example
 * // returns "erc.what"
 * normalizeHeader("erc.what", "erc")
 *
 * @example
 * // returns "datacite_resourcetype"
 * normalizeHeader("Resource Type", "datacite")
 *
 * @example
 * // returns "_target"
 * normalizeHeader("location", "dc")
 *
 * @example
 * // returns "nonstandard"
 * normalizeHeader("nonstandard", "dc")
 *
 * @param {string} cell
 * @param {String} profile The metadata profile ('datacite', 'dc', or 'erc')
 * @returns {string}
 */
function normalizeHeader(cell, profile) {
  if (typeof(headers[profile]) === 'undefined')
    throw new Error(`Invalid profile ${profile}`)

  const profileRegexp = new RegExp(`^${profile}[\._]`, 'g')
  const normalizedCell = cell.trim().replace(profileRegexp, '').toLowerCase()

  const standardHeaders = Object.keys(headers[profile.toLowerCase()])

  let newValue
  // EzID expects 'target' as the header for URI fields, where the common
  // practice is to put 'location'
  if (normalizedCell === 'location')
    newValue = '_target'
  // append the profile if it is one of the standard headers
  else if (standardHeaders.indexOf(normalizedCell) > -1)
    newValue = `${profile}.${normalizedCell}`
  else
    newValue = normalizedCell

  return newValue
}

/**
 * Get the header cells from a sheet
 *
 * @param {Object} sheet SheetJS worksheet
 * @param {number} columns
 * @returns {Array}
 */
function getHeaders(sheet, columns) {
  // Object.entries converts associative arrays to regular arrays with
  // the key as the first element, so we can use native array methods
  // on them
  Object.entries(sheet).filter(value => {
    // reject the metadata fields, whose keys start with '!'
    return value[0].indexOf('!') !== 0
  }).slice(0, columns)
}

/**
 * @example
 * // return value
 *  [ { _target: '',
 *     erc.what: 'A hat',
 *     erc.who: 'Haberdasher',
 *     erc.when: 2015 },
 *   { _target: 'https://twitter.com',
 *     erc.what: 'The worst website',
 *     erc.who: 'Online',
 *     erc.when: 'forever' } ]
 *
 * @param {Object} sheet SheetJS worksheet
 * @param {string} profile
 * @returns {Array<Object>}
 */
module.exports = function (sheet, profile) {
  const range = parseRange(sheet['!ref'])
  const headers = getHeaders(sheet, range.columns)

  // SheetJS doesn't group cells into rows, instead indexing them by
  // cell number.  But now that we have the row count, we can do
  // that ourselves.
  //
  // We drop the first row, which is the headers
  const rows = range.rows.slice(1).map(rowN => {
    return range.columns.map(columnN => {
      const value = sheet[`${columnN}${rowN}`]

      return typeof(value) === 'undefined' ? '' : value.w
    })
  })

  return rows.map(row => {
    const profiled_object = {}

    row.forEach((cell, index, _array) => {
      // get the header cell corresponding to this row cell
      const headerKey = `${range.columns[index]}${range.rows[0]}`

      // for keys
      const ezidHeader = normalizeHeader(sheet[headerKey].w, profile)
      profiled_object[ezidHeader] = cell
    })

    return profiled_object
  })
}
