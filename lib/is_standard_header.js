const headers = require('./render/tables/headers.js')
const util = require('util')

module.exports = isStandardHeader

/**
 * @param {string} profile One of 'datacite', 'dc', 'erc'
 * @param {string} header
 * @returns {boolean}
 */
function isStandardHeader (profile, header) {
  if (typeof(header) === 'undefined')
    return false

  const goodProfile = ['datacite', 'dc', 'erc'].some(value => {
    return value === profile.toLowerCase()
  })

  if (!goodProfile)
    throw new Error(`Invalid profile ${profile}`)

  const standard = Object.assign({}, headers[profile.toLowerCase()], headers.generic.GENERIC)
  return Object.keys(standard).some(value => {
    const simplifiedHeader = header.replace(/^erc[\._]+/, '')
          .replace(/^datacite[\._]+/, '')
          .replace(/^dc[\._]+/, '')
          .replace(/\ /g, '')

    return value === simplifiedHeader
  })
}
