/**
 * @param {Object} hash
 * @return {Object} The transformed hash
 */
module.exports = function (hash) {
  if (!hash.datacite)
    return hash

  const datacite = hash.datacite.map(hash => {
    const cleaned = {}

    Object.keys(hash).forEach(key => {
      if (key === 'datacite.resourcesubtype')
        return

      if (key === 'datacite.resourcetype')
        cleaned[key] = `${hash['datacite.resourcetype']}/${hash['datacite.resourcesubtype']}`
      else
        cleaned[key] = hash[key]
    })

    return cleaned
  })

  return Object.assign({}, hash, { datacite: datacite })
}
