const util = require('util')
const replaceKeys = require('./replace_keys.js')

module.exports = {
  /**
   * Sanitize a record object for saving it to the database
   * @param {Object} hash
   * @returns {Object}
   */
  in: function (hash) {
    return safeHash('encode', replaceKeys(hash, /\./g, '_'))
  },
  /**
   * Parse a DB-sanitized object and restore it to its original state
   * @param {Object} hash
   * @returns {Object}
   */
  out: function (hash) {
    return safeHash('decode', hash)
  }
}

/**
 * Either encode a hash for DB-safety, or decode a DB-safe hash
 * @param {string} direction Either 'encode' or 'decode'
 * @param {Object} hash
 * @returns {Object}
 */
function safeHash (direction, hash) {
  if (! (direction === 'encode' || direction === 'decode') )
    throw new Error(`safeHash(): bad direction parameter ${direction}`)

  const URIMethod = (direction === 'encode')
        ? encodeURIComponent
        : decodeURIComponent

  const newHash = {}

  // console.log(`in (${direction})`)
  // console.log(util.inspect(hash))

  Object.keys(hash).forEach(key => {
    if (typeof(hash[key]) === 'undefined' || hash[key] === null)
      newHash[URIMethod(key)] = ''
    else if (typeof(hash[key]) !== 'object')
      newHash[URIMethod(key)] = URIMethod(hash[key])
    else if (Array.isArray(hash[key]))
      newHash[URIMethod(key)] = hash[key].map(value => { return URIMethod(value) })
    else
      newHash[URIMethod(key)] = safeHash(direction, hash[key])
  })

  // console.log(`out (${direction})`)
  // console.log(util.inspect(newHash))

  return newHash
}
