const sanitize = require('./sanitize_db_hash.js')
const isStandardHeader = require('../is_standard_header.js')

module.exports = function (hash, profile) {
  const dirty = sanitize.out(hash)
  const cleaned = {}

  // remove database keys like createdAt
  Object.keys(dirty).forEach(header => {
    if (!isStandardHeader(profile, header))
      return

    if (header === 'id')
      return

    if (header === '_target')
      cleaned['location'] = dirty[header]
    else if (header === 'ark')
      cleaned['ARK'] = dirty[header]
    else
      cleaned[header.replace(/_/g, '.')] = dirty[header]
  })

  // unpack nonstandard_headers JSON blob into first-class headers
  Object.keys(dirty.nonstandard_headers || {}).forEach(header => {
    cleaned[header] = dirty.nonstandard_headers[header]
  })

  return cleaned
}
