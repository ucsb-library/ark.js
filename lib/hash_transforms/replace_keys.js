/**
 * Perform the specified regexp transformation on the keys of the provided hash
 * @param {Object} hash
 * @param {Regexp} regexp
 * @param {string} replacement
 * @return {Object} The transformed hash
 */
module.exports = function (hash, regexp, replacement) {
  const cleaned = {}

  Object.keys(hash).forEach(key => {
    const cleanKey = key.replace(regexp, replacement)

    cleaned[cleanKey] = hash[key]
  })

  return cleaned
}
