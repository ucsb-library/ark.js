/**
 * Given a set of new objects from an uploaded spreadsheet, and a set of
 * previously minted objects that are now persisted in the database, return a
 * hash with a `create` key containing original objects, and an `update` key
 * with objects from {fresh} that already exist in {old}
 *
 * @param {Array<Object>} fresh New objects extracted from uploaded spreadsheets
 * @param {Array<Object>} old Objects already persisted in the database
 * @return {{ create: Array<Object>, update: Array<Object> }}
 */
module.exports = function (fresh, old) {
  const newAndOld = {
    create: [],
    update: []
  }

  fresh.forEach(candidate => {
    // prioritize ARK matches against target matches
    const arkMatch = old.filter(dbObj => {
      if (!candidate.ark)
        return false

      return candidate.ark === dbObj.ark
    })

    if (arkMatch.length > 0)
      return newAndOld.update.push(
        Object.assign(
          // don't add ID and ARK keys if they aren't defined
          (arkMatch[0].id ? { id: arkMatch[0].id } : {}),
          (arkMatch[0].ark ? { ark: arkMatch[0].ark } : {}),
          candidate
        )
      )

    const targetMatch = old.filter(dbObj => {
      if (!candidate._target)
        return false

      return candidate._target === dbObj._target
    })

    if (targetMatch.length > 0)
      return newAndOld.update.push(
        Object.assign(
          // don't add ID and ARK keys if they aren't defined
          (targetMatch[0].id ? { id: targetMatch[0].id } : {}),
          (targetMatch[0].ark ? { ark: targetMatch[0].ark } : {}),
          candidate
        )
      )
    else
      return newAndOld.create.push(candidate)
  })

  return newAndOld
}
