const generateEditTable = require('../render/tables/generate.js')
const queueJobs = require('../jobs/queueJobs.js')
const util = require('util')

const approot = (typeof(PhusionPassenger) !== 'undefined')
      ? '/var/www'
      : `${__dirname}/../..`
const current = (typeof(PhusionPassenger) !== 'undefined')
      ? `${approot}/current`
      : approot

/**
 * @param {Object} req Request object from Express
 * @param {Object} res Response object to send through Express
 */
module.exports = function (req, res) {
  queueJobs({
    email: req.body.email,
    jobs: req.body.update,
    profile: req.body._profile,
    queue: 'update',
    user: req.user
  })

  // if the user provided a spreadsheet with some objects that have been already
  // minted as well as some new ones, they will first be prompted to update the
  // metadata for the pre-existing objects.  After that, we send them through
  // the new-object flow
  if (req.body['create']) {
    const toMint = {
      update: [],
      create: JSON.parse(req.body['create'][req.body._profile])
    }

    return res.render(
      `${current}/views/edit`,
      generateEditTable({ sorted: toMint,
                          profile: req.body._profile,
                          user: req.user })
    )
  } else {
    return res.render(
      `${current}/views/success`,
      {
        title: 'Success'
      }
    )
  }
}
