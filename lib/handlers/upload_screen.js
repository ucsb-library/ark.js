const approot = (typeof(PhusionPassenger) !== 'undefined')
      ? '/var/www'
      : `${__dirname}/../..`
const current = (typeof(PhusionPassenger) !== 'undefined')
      ? `${approot}/current`
      : approot

/**
 * @param {Object} req Request object from Express
 * @param {Object} res Response object to send through Express
 */
module.exports = function (req, res) {
  if (req.user)
    return res.render(
      `${current}/views/upload`,
      {
        title: 'Upload',
        user: req.user
      }
    )

  return res.redirect('/')
}
