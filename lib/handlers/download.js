const DB = require('../../db/models/index.js')
const formatDbOutput = require('../hash_transforms/format_db_output.js')
const moment = require('moment')
const util = require('util')
const XLSX = require('xlsx')

const approot = (typeof(PhusionPassenger) !== 'undefined')
      ? '/var/www'
      : `${__dirname}/../..`
const current = (typeof(PhusionPassenger) !== 'undefined')
      ? `${approot}/current`
      : approot

/**
 * @param {Object} req Request object from Express
 * @param {Object} res Response object to send through Express
 */
module.exports = function (req, res) {
  const user = req.query.user || req.query.job_id.replace(/_.*/, '')
  if (req.user !== user)
    return res.render(`${current}/views/upload`,
                      {
                        flash: ['Unauthorized request; you can only access your own jobs'],
                        title: 'Upload',
                        user: req.user
                      })

  const constraint = req.query.user
        ? { created_by: user }
        : { job_id: req.query.job_id }
  const query = { where: Object.assign({ '_profile': req.query.profile }, constraint) }

  return DB.Metadatum.findAll(query).then(results => {
    const records = [].concat(...results.map(value => {
      return formatDbOutput(value.dataValues, req.query.profile)
    }))

    // https://github.com/SheetJS/js-xlsx/issues/163#issuecomment-289184799
    const book = {
      SheetNames: ["Sheet1"],
      Sheets: {
        "Sheet1": XLSX.utils.json_to_sheet(records)
      }
    }
    const filename = req.query.user
          ? `${req.query.profile}_ALL_${Date.now()}.xlsx`
          : `${req.query.profile}_${moment(parseInt(req.query.job_id.replace(/.*_/, ''))).format('YYYY-MM-DD_HH.mm.ss')}.xlsx`

    res.set("Content-Disposition", `attachment; filename="${filename}"`)
    return res.send(XLSX.write(book, { type: 'buffer' }))
  })
}
