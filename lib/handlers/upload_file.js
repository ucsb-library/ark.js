const Busboy = require('busboy')
const DB = require('../../db/models/index.js')
const generateEditTable = require('../render/tables/generate.js')
const isStandardHeader = require('../is_standard_header.js')
const parseSheet = require('../parse_sheet.js')
const sanitize = require('../hash_transforms/sanitize_db_hash.js')
const sortDuplicates = require('../sort_duplicates.js')
const util = require('util')
const XLSX = require('xlsx')

const approot = (typeof(PhusionPassenger) !== 'undefined')
      ? '/var/www'
      : `${__dirname}/../..`
const current = (typeof(PhusionPassenger) !== 'undefined')
      ? `${approot}/current`
      : approot

/**
 * @param {Object} req Request object from Express
 * @param {Object} res Response object to send through Express
 */
module.exports = function (req, res) {
  // 5MiB max upload size
  //
  // TODO: lower this limit after testing
  if (req.headers['content-length'] > 5000000)
    return res.sendStatus('400')

  // TODO: reject bad mime types
  // console.log(util.inspect(req.headers))

  const boy = new Busboy({ headers: req.headers })

  let buffer,
      profile,
      data = []

  boy.on('file', (_fieldname, file, filename, encoding, mimetype) => {
    console.log(`Uploading ${filename}...`)

    file.on('data', chunk => {
      data.push(chunk)
    })
  })

  boy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
    if (fieldname === '_profile')
      profile = val
  })

  boy.on('finish', () => {
    console.log(`${data.length} bytes received.`)

    const sheets = XLSX.read(Buffer.concat(data), { type: 'array' }).Sheets
    // Fetch the first sheet that has a defined range
    //
    // TODO: support multi-sheet workbooks
    const sheet = Object.values(sheets).filter(sheet => {
      return Object.keys(sheet).some(value => { return value === '!ref' })
    })[0]

    // console.log(util.inspect(sheet))

    const fresh = parseSheet(sheet, profile)

    DB.Metadatum.findAll({
      where: { created_by: req.user }
    }).then(result => {
      const old = result.map(value => { return sanitize.out(value.dataValues) })
      const sorted = sortDuplicates(fresh, old)

      return res.render(
        `${current}/views/edit`,
        generateEditTable({ sorted: sorted, profile: profile, user: req.user })
      )
    })
  })

  return req.pipe(boy)
}
