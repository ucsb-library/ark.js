const DB = require('../../db/models/index.js')
const makeArchive = require('../render/archive.js')
const sanitize = require('../hash_transforms/sanitize_db_hash.js')
const util = require('util')

const approot = (typeof(PhusionPassenger) !== 'undefined')
      ? '/var/www'
      : `${__dirname}/../..`
const current = (typeof(PhusionPassenger) !== 'undefined')
      ? `${approot}/current`
      : approot

/**
 * @param {Object} req Request object from Express
 * @param {Object} res Response object to send through Express
 */
module.exports = function (req, res) {
  return DB.Metadatum.findAll({
    where: { created_by: req.user }
  }).then(results => {
    const unescaped = results.map(value => { return sanitize.out(value.dataValues) })
    const allObjects = sortObjects(unescaped)

    return res.render(
      `${current}/views/archive`,
      {
        archive: makeArchive(allObjects),
        title: 'Archive',
        user: req.user
      }
    )
  }).catch(err => {
    console.error(util.inspect(err))

    return res.render(
      `${current}/views/archive`,
      {
        archive: '',
        flash: [`Could not retrieve archive (${err.message}); please contact the site admin.`],
        title: 'Archive',
        user: req.user
      }
    )
  })
}

/**
 * @param {Object} dbResult Database object from Sequelize
 * @returns {{ datacite: Object, dc: Object, erc: Object }}
 */
function sortObjects (dbResult) {
  const allObjects = {}

  // FIXME: why does this need to be assigned
  const profs = ['datacite', 'dc', 'erc']
  profs.forEach(profile => {
    allObjects[profile] = {}

    // console.log(profile)
    const byProfile = dbResult.filter(value => {
      return value._profile === profile
    }) || {}

    byProfile.forEach(job => {
      if (!allObjects[profile][job.job_id])
        allObjects[profile][job.job_id] = []

      allObjects[profile][job.job_id].push(job)
    })
  })

  // end result should be
  // { datacite: {},
  //   dc: {},
  //   erc: { user@host_jobTime: [
  //     { id: 1,
  //       ark: 'ark:/99999/fk40c65h6z',
  //       _profile: 'erc',
  //       erc_who: 'new yorkers',
  //       erc_what: 'english muffin',
  //       erc_when: '1859',
  //       dc_creator: null,
  //       dc_title: null,
  //       dc_type: null,
  //       dc_publisher: null,
  //       dc_date: null,
  //       _target: '',
  //       datacite_creator: null,
  //       datacite_title: null,
  //       datacite_resourcetype: null,
  //       datacite_publisher: null,
  //       datacite_publicationyear: null,
  //       created_by: 'user@host',
  //       job_id: 'user@host_jobTime',
  //       nonstandard_headers: null,
  //       createdAt: 2018-04-10T21:54:18.751Z,
  //       updatedAt: 2018-04-10T21:54:18.751Z },
  //     { id: 2,
  //       ark: 'ark:/99999/fk4vq45823',
  //       _profile: 'erc',
  //       erc_who: '',
  //
  // etc.
  return allObjects
}
