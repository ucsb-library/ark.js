const mergeResourcetypes = require('../hash_transforms/merge_resourcetypes.js')
const queueJobs = require('../jobs/queueJobs.js')

const approot = (typeof(PhusionPassenger) !== 'undefined')
      ? '/var/www'
      : `${__dirname}/../..`
const current = (typeof(PhusionPassenger) !== 'undefined')
      ? `${approot}/current`
      : approot

/**
 * @param {Object} req Request object from Express
 * @param {Object} res Response object to send through Express
 */
module.exports = function (req, res) {
  queueJobs({
    email: req.body.email,
    jobs: mergeResourcetypes(req.body.create),
    profile: req.body._profile,
    queue: 'create',
    user: req.user
  })

  return res.render(
    `${current}/views/success`,
    {
      title: 'Success'
    }
  )
}
