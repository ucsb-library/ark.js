const util = require('util')

const approot = (typeof(PhusionPassenger) !== 'undefined')
      ? '/var/www'
      : `${__dirname}/../..`
const current = (typeof(PhusionPassenger) !== 'undefined')
      ? `${approot}/current`
      : approot

/**
 * @param {Object} req Request object from Express
 * @param {Object} res Response object to send through Express
 */
module.exports = function (req, res) {
  // console.log(util.inspect(req))

  // this is a destructive function; after calling it the flashes are
  // reset
  const flashes = req.flash("error")

  if (req.user)
    return res.redirect('/upload')

  return res.render(
    `${current}/views/login`,
    {
      flash: flashes,
      title: 'Login'
    }
  )
}
