module.exports = {
  archive: require('./handlers/archive.js'),
  create: require('./handlers/create.js'),
  download: require('./handlers/download.js'),
  root: require('./handlers/root.js'),
  update: require('./handlers/update.js'),
  uploadFile: require('./handlers/upload_file.js'),
  uploadScreen: require('./handlers/upload_screen.js')
}
