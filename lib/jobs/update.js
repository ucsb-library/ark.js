const Bull = require('bull')
const util = require('util')
const db = require('../../db/models/index.js')
const sanitize = require('../hash_transforms/sanitize_db_hash.js')

const EzID = require('ezid')
const client = new EzID(require('../../config/ezid.js'))

/**
 * @param {Object} job Bull job parameters
 * @param {Function} done Bull callback
 */
module.exports = function (job, done) {
  const emailJob = new Bull('email')

  updateAndSave(job.data).then(values => {
    emailJob.add(
      Object.assign(
        { subject: "Your ARKs have been updated",
          body: "The ARKs for the following objects were successfully modified:\n\n" +
          util.inspect(job.data.object) },
        job.data
      )
    )

    return done()
  }).catch(err => {
    emailJob.add(
      Object.assign(
        { subject: "An ARK minting job failed",
          body: "The ARK Minter failed to mint an ARK for the following object:\n\n" +
          util.inspect(job.data.object) },
        job.data
      )
    )
    return done(err)
  })
}

/**
 * @param {{ object: Object, email: string, jobId: string, user: string }} hash Provided by queueJobs
 * @returns {Promise}
 */
function updateAndSave (hash) {
  return client.update({ id: hash.object.ark, metadata: hash.object }).then(res => {
    if (res.id === undefined)
      throw new Error(`ARK ${hash.object.ark} does not exist, cannot update object`)

    return db.Metadatum.findOne({
      where: { ark: encodeURIComponent(res.id), created_by: hash.user }
    }).then(item => {
      // we can't escape the data earlier because then it will be escaped in EzID
      item.update(sanitize.in(hash.object))
    })
  })
}
