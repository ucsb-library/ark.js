const Bull = require('bull')
const db = require('../../db/models/index.js')
const sanitize = require('../hash_transforms/sanitize_db_hash.js')
const util = require('util')

const EzID = require('ezid')
const client = new EzID(require('../../config/ezid.js'))

/**
 * @param {Object} job Bull job parameters
 * @param {Function} done Bull callback
 */
module.exports = function (job, done) {
  const emailJob = new Bull('email')

  mintAndSave(job.data).then(_values => {
    emailJob.add(
      Object.assign(
        { subject: "Your new ARKs have been minted",
          body: "ARKs were successfully created for the following objects:\n\n" +
          util.inspect(job.data.object) },
        job.data
      )
    )

    return done()
  }).catch(err => {
    emailJob.add(
      Object.assign(
        { subject: "An ARK minting job failed",
          body: "The ARK Minter failed to mint an ARK for the following object:\n\n" +
          util.inspect(job.data.object) },
        job.data
      )
    )
    return done(err)
  })
}

/**
 * @param {{ object: Object, email: String, jobId: String, user: String }} hash Provided by queueJobs
 * @returns {Promise}
 */
function mintAndSave (hash) {
  return client.mint(hash.object).then(res => {
    return db.Metadatum.create(
      Object.assign(
        { ark: res.id,
          created_by: hash.user,
          job_id: hash.jobId },
        // we can't escape the data earlier because then it will be escaped in EzID
        sanitize.in(hash.object)
      )
    )
  })
}
