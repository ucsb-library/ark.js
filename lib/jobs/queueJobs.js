const Bull = require('bull')
const util = require('util')

/**
 * Parse and queue minting jobs
 * @param {{ jobArray: Array<Object>, queueName: string, profile: string, user: string }} options
 * @returns {undefined}
*/
module.exports = function (options) {
  // group the objects from each spreadsheet for the archive
  const jobId = `${options.user}_${Date.now()}`

  demangle(options.jobs, options.profile).forEach(hash => {
    const jobHash = {
      object: hash,
      email: options.email,
      jobId: jobId,
      user: options.user
    }

    const job = new Bull(options.queue)
    job.add(jobHash)
  })
}

/**
 * @param {Object} body Request body passed from Express
 * @param {string} profile
 * @returns {Array}
 */
function demangle (body, profile) {
  const fresh = body[profile].map(hash => {
    return Object.assign({ '_profile': profile }, hash)
  })

  // https://stackoverflow.com/a/34980419
  return [].concat(...fresh)
}
