const Email = require('../../config/email.js')
const util = require('util')
const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
  auth: Email.auth,
  host: Email.host,
  port: Email.port,
  secure: Email.secure
})

/**
 * @param {Object} job Bull job parameters
 * @param {Function} done Bull callback
 */
module.exports = function (job, done) {
  const missive = {
    from: Email.from,
    to: job.data.email,
    subject: job.data.subject,
    text: job.data.body
  }

  transporter.sendMail(missive, (err, info) => {
    if (err)
      return done(err)

    return done(null, info)
  })
}
