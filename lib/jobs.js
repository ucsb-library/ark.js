module.exports = {
  create: require('./jobs/create.js'),
  mail: require('./jobs/mail.js'),
  update: require('./jobs/update.js')
}
