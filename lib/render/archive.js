const isStandardHeader = require('../is_standard_header.js')
const headers = require('./tables/headers.js')
const moment = require('moment')
const util = require('util')
const NAMED_PROFILES = headers.generic.NAMED_PROFILES

/**
 * @param {{ datacite: Object, dc: Object, erc: Object }} objects
 * @returns {string} HTML to be rendered
 */
module.exports = function (objects) {
  return ['datacite', 'dc', 'erc'].map(profile => {
    return downloadProfile(objects[profile], profile)
  }).join('')
}

/**
 * @param {Object} objects All records for the given profile
 * @param {string} profile
 * @returns {string} HTML
 */
function downloadProfile (objects, profile) {
  if (typeof(profile) === 'undefined')
    throw new Error("No profile provided to downloadProfile()")

  // console.log(util.inspect(objects))

  if (Object.keys(objects).length < 1)
    return ''

  const header = `<h2>${NAMED_PROFILES[profile]}</h2>`
  const allObjects = downloadAllProfile(objects, profile)
  const eachJob = downloadProfileJobs(objects, profile)

  return header + allObjects + eachJob
}

/**
 * Generate HTML table and form to download all records for a given profile
 *
 * @param {Object} objects All records for the given profile
 * @param {string} profile
 * @returns {string} HTML
 */
function downloadAllProfile (objects, profile) {
  return "<form action='/download'>" +
    `<input type='hidden' name='profile' value='${profile}'>` +
    `<input type='hidden' name='user' value='${Object.values(objects)[0][0].created_by}'>` +
    `<input type='submit' value='Download all ${NAMED_PROFILES[profile]} records'>` +
    '</form>'
}

/**
 * Generate HTML tables and forms to download each batch job for a given profile
 *
 * @param {Object} objects All records for the given profile
 * @param {string} profile
 * @returns {string} HTML
 */
function downloadProfileJobs (objects, profile) {
  return Object.keys(objects).map(job_key => {
    const time = moment(parseInt(job_key.replace(/.*_/, '')))

    const form =
          "<form action='/download'>" +
          `<input type='hidden' name='profile' value='${profile}'>` +
          `<input type='hidden' name='job_id' value='${job_key}'>` +
          `<h3>Job: ${time.format('YYYY-MM-DD HH:mm:ss')}</h3>` +
          `<input type='submit' value="Download this job">` +
          '</form>'

    const thead = "<table class='minted'><thead><tr>" +
          '<th></th>' +
          Object.keys(headers[profile]).map(value => {
            return `<th>${headers[profile][value].long}</th>`
          }).join('') +
          '<th>Location</th><th>ARK</th>' +
          Object.keys(objects[job_key][0].nonstandard_headers || {}).map(value => {
            return `<th>${value}</th>`
          }).join('') +
          '</tr></thead><tbody>'

    const tbody = objects[job_key].map(job => {
      return '<tr><td class="line_number"></td>' +
        Object.keys(headers[profile]).map(value => {
          return '<td>' + job[`${profile}_${value}`] + '</td>'
        }).join('') +
        '<td>' + (job._target
                  ? `<a href='${job._target}'>${job._target}</a>`
                  : job._target) + '</td>' +
        `<td><a href="http://ezid.cdlib.org/id/${job.ark}" title="ARK">${job.ark}</a></td>` +
        Object.keys(objects[job_key][0].nonstandard_headers || {}).map(value => {
          return `<td>${job.nonstandard_headers[value]}</td>`
        }).join('') +
        '</tr>'
    }).join('') + '</tbody></table>'

    return form + thead + tbody
  }).join('')
}
