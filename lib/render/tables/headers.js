module.exports = {
  datacite: require('./headers/datacite.js'),
  dc: require('./headers/dc.js'),
  erc: require('./headers/erc.js'),
  generic: require('./headers/generic.js')
}
