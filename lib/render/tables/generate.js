const headers = require('./headers.js')
const isStandardHeader = require('../../is_standard_header.js')
const NAMED_PROFILES = require('./headers.js').generic.NAMED_PROFILES
const replaceKeys = require('../../hash_transforms/replace_keys.js')
const util = require('util')

/**
 * Generate a hash containing the HTML elements used for rendering the pre-mint
 * record editing view
 *
 * @param {{ sorted: { create: Array, update: Array }, profile: string, user: string}} options
 * @returns {Object}
 */
module.exports = function (options) {
  // console.log(util.inspect(options))

  const sorted = options.sorted
  const profile = options.profile
  const user = options.user

  const status = sorted.update.length > 0 ? 'update' : 'create'

  const nonstandardColumns = Object.keys(sorted[status][0]).filter(value => {
    return !isStandardHeader(profile, value)
  })

  const columns = generateHeaders(profile)
        .concat(['_target', 'ark', 'id'])
        .concat(nonstandardColumns)

  return {
    entries: entriesFor(status, profile, columns, sorted),
    htmlclass: 'table',
    extraHeaders: extraHeaders(nonstandardColumns),
    profile_name: NAMED_PROFILES[profile],
    standardHeaders: standardHeadersFor(profile),
    profile: profile,
    status: status,
    title: 'Edit',
    user: user
  }
}

/**
 * @param {string} profile One of 'datacite', 'dc', 'erc'
 * @returns {array}
 */
function generateHeaders (profile) {
  const goodProfile = ['datacite', 'dc', 'erc'].some(value => {
    return value === profile.toLowerCase()
  })

  if (!goodProfile)
    throw new Error(`Invalid profile ${profile}`)

  return Object.keys(headers[profile.toLowerCase()]).map(value => {
    return `${profile}.${value}`
  })
}

/**
 * Generate the HTML table headers for the given profile
 *
 * @param {string} profile
 * @returns {string}
 */
function standardHeadersFor(profile) {
  if (typeof(headers[profile]) === 'undefined')
    throw new Error(`Invalid profile ${profile}`)

  const profileHeaders = Object.assign(
    {},
    headers[profile.toLowerCase()],
    headers.generic.GENERIC // FIXME
  )

  return Object.keys(profileHeaders).map(key => {
    return `<th class='standard_header ${profileHeaders[key].header_classes.join(' ')}' ` +
      headerAttributes(profileHeaders[key].header_attributes) +
      `>${profileHeaders[key].long}</th>`
  }).join('')
}

/**
 * Generate HTML attributes for header columns
 * @param {Object} attributes
 * @returns {string}
 */
function headerAttributes (attributes) {
  return Object.keys(attributes || {}).map(key => {
    return `${key}='${attributes[key]}'`
  }).join(' ')
}

/**
 * Generate HTML table headers for any extra (nonstandard) fields
 *
 * @param {Array} extras
 * @returns {string}
 */
function extraHeaders(extras) {
  return extras.filter(value => { return typeof(value) !== 'undefined' }).map(value => {
    return `<th class='${encodeURIComponent(value)}'>${value}</th>`
  }).join('')
}

/**
 * Generate the HTML table elements for the provided metadata
 *
 * @param {string} status Either 'create' or 'update'
 * @param {string} profile
 * @param {Array<string>} columns
 * @param {Array<Object>} rows
 * @returns {string}
 */
function entriesFor(status, profile, columns, rows) {
  const visible = rows[status].map((row, index) => {
    // run the defined transforms on each cell to generate table data
    const cells = columns.map(col => {
      const colHeaders = Object.assign(
        {},
        headers.generic.GENERIC,
        headers[profile.toLowerCase()]
      )

      if (isStandardHeader(profile, col)) {
        const profileRegexp = new RegExp(`^${profile}[\._]`, 'g')
        const key = col.replace(profileRegexp, '')

        const strippedRow = replaceKeys(
          replaceKeys(row, /\ /g, ''),
          profileRegexp,
          ''
        )

        return colHeaders[key].transform(
          status,
          profile,
          index,
          strippedRow[key]
        )
      }

      // if it is a nonstandard header:
      return `<td><input name="${status}[${profile}][${index}][nonstandard_headers][${col}]" value="${row[col]}"></td>`
    }).filter(value => { return value !== null })

    return '<tr><td class="line_number"></td>' +
      cells.join('') +
      '<td><button name="button" class="delete">✘</button></td>' +
      "</tr>"
  }).join('')

  // if some of the uploaded objects already exist in the database, we prompt
  // the user to update their metadata first; but afterwards we need to let them
  // finish minting the objects that aren't duplicates.  For that we need to
  // hold on to those objects, but not render them
  let hidden = ''
  if (status === 'update' && rows['create'].length > 0)
    hidden = `<input type='hidden' name='create[${profile}]' value='${JSON.stringify(rows['create'])}'>`

  return visible + hidden
}
