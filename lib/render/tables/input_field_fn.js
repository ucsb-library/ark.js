/**
 * Generate a transform function for producing HTML table cells
 *
 * @param {string} inputType The HTML input type, e.g. 'text', 'hidden', etc.
 * @param {string} fieldName The name of the column header that the function will be generating HTML cells for
 * @returns {Function}
 */
module.exports = function(inputType, fieldName) {
  return function(status, profile, index, cellValue) {
    // console.log(`callVAelys: ${cellValue} (${typeof(cellValue)})`)

    const cleanValue = typeof(cellValue) === 'undefined'
          ? ''
          : cellValue.toString().trim()

    return `<td><input class='${fieldName}' ` +
      `type='${inputType}' ` +
      `name='${status}[${profile}][${index}][${fieldName}]' ` +
      `value='${cleanValue}' ` +
      "placeholder=''></td>"
  }
}
