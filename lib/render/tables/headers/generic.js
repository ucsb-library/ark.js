const DATACITE = require('./datacite.js')
const DC = require('./dc.js')
const ERC = require('./erc.js')
const inputFieldFn = require('../input_field_fn.js')

const NAMED_PROFILES = {
  datacite: 'DataCite',
  dc: 'DublinCore',
  erc: 'ERC'
}

const GENERIC = {
  _target: {
    long: 'Location',
    header_classes: [],
    transform: inputFieldFn('url', '_target')
  },
  ark: {
    long: 'ARK',
    header_classes: [],
    /**
     * Generate an HTML table cell for this column header
     *
     * @param {string} status
     * @param {string} profile
     * @param {number} index
     * @param {string} cellValue
     * @returns {string}
     */
    transform: function (status, profile, index, cellValue) {
      if (!cellValue)
        return "<td><span class='ark'></span></td>"

      return '<td>' +
        `<a class='ark' href='http://ezid.cdlib.org/id/${cellValue}'>` +
        cellValue + '</a>' +
        `<input type='hidden' name=${status}[${profile}][${index}][ark] value='${cellValue}'>`+
        '</td>'
    }
  },
  id: {
    long: '',
    header_classes: ['id', 'hidden'],
    transform: inputFieldFn('hidden', 'id')
  }
}

const CONTROLLED_HEADERS =
      Object.keys(DATACITE)
      .concat(Object.keys(DC))
      .concat(Object.keys(ERC))
      .concat(Object.keys(GENERIC))
      .concat([
        'created_by',
        'job_id',
        'nonstandard_headers',
        '_profile',
        'resource',
        'type',
      ])

const EZID_HEADERS =
      CONTROLLED_HEADERS.filter(value => {
        return ['created_by',
                'id',
                'job_id',
                'nonstandard_headers'
               ].indexOf(value) === -1
      })

module.exports = {
  CONTROLLED_HEADERS,
  EZID_HEADERS,
  GENERIC,
  NAMED_PROFILES
}
