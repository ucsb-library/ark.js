const inputFieldFn = require('../input_field_fn.js')

module.exports = {
  what: {
    long: 'What',
    header_classes: [],
    transform: inputFieldFn('text', 'erc.what')
  },
  who: {
    long: 'Who',
    header_classes: [],
    transform: inputFieldFn('text', 'erc.who')
  },
  when: {
    long: 'When',
    header_classes: [],
    transform: inputFieldFn('text', 'erc.when')
  }
}
