const inputFieldFn = require('../input_field_fn.js')

// Controlled vocabulary for DataCite resource types:
// http://ezid.cdlib.org/doc/apidoc.html#profile-datacite
const RESOURCE_TYPES = [
  'Audiovisual',
  'Collection',
  'Dataset',
  'Event',
  'Image',
  'InteractiveResource',
  'Model',
  'PhysicalObject',
  'Service',
  'Software',
  'Sound',
  'Text',
  'Workflow',
  'Other'
]

module.exports = {
  title: {
    long: 'Title',
    header_classes: [],
    transform: inputFieldFn('text', 'datacite.title')
  },
  creator: {
    long: 'Creator',
    header_classes: [],
    transform: inputFieldFn('text', 'datacite.creator')
  },
  resourcetype: {
    long: 'Resource Type',
    header_classes: [],
    header_attributes: { colspan: '2' },
    /**
     * Generate an HTML table cell for this column header
     *
     * @param {string} status
     * @param {string} profile
     * @param {number} index
     * @param {string} cellValue
     * @returns {string}
     */
    transform: function (status, profile, index, cellValue) {
      const [supertype, subtype] = (cellValue || '').split('/')

      // console.log(`super ${supertype}`)
      // console.log(`sub ${subtype}`)

      const pulldowns = RESOURCE_TYPES.map(value => {
        const isSelected = supertype.toLowerCase() === value.toLowerCase()

        return `<option value='${value}' ${(isSelected ? " selected" : " ")}>${value}</option>`
      })

      const supertypeField = "<td class='datacite.resourcetype'>" +
            `<select name='${status}[${profile}][${index}][datacite.resourcetype]'>` +
            pulldowns +
            "</select></td>"

      const subtypeField = "<td>" +
            "<input class='datacite.resourcesubtype' " +
            "type='text' " +
            `name='${status}[${profile}][${index}][datacite.resourcesubtype]' ` +
            `value='${(subtype || '')}' ` +
            "placeholder='/ resource subtype'" +
            "</td>"

      return supertypeField + subtypeField
    }
  },
  publicationyear: {
    long: 'Publication Year',
    header_classes: [],
    transform: inputFieldFn('text', 'datacite.publicationyear')
  },
  publisher: {
    long: 'Publisher',
    header_classes: [],
    transform: inputFieldFn('text', 'datacite.publisher')
  }
}
