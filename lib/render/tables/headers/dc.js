const inputFieldFn = require('../input_field_fn.js')

module.exports = {
  title: {
    long: 'Title',
    header_classes: [],
    transform: inputFieldFn('text', 'dc.title')
  },
  creator: {
    long: 'Creator',
    header_classes: [],
    transform: inputFieldFn('text', 'dc.creator')
  },
  type: {
    long: 'Type',
    header_classes: [],
    transform: inputFieldFn('text', 'dc.type')
  },
  date: {
    long: 'Date',
    header_classes: [],
    transform: inputFieldFn('text', 'dc.date')
  },
  publisher: {
    long: 'Publisher',
    header_classes: [],
    transform: inputFieldFn('text', 'dc.publisher')
  }
}
