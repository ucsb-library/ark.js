'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Metadata', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ark: {
        type: Sequelize.STRING
      },
      _profile: {
        type: Sequelize.STRING
      },
      erc_who: {
        type: Sequelize.STRING
      },
      erc_what: {
        type: Sequelize.STRING
      },
      erc_when: {
        type: Sequelize.STRING
      },
      dc_creator: {
        type: Sequelize.STRING
      },
      dc_title: {
        type: Sequelize.STRING
      },
      dc_type: {
        type: Sequelize.STRING
      },
      dc_publisher: {
        type: Sequelize.STRING
      },
      dc_date: {
        type: Sequelize.STRING
      },
      _target: {
        type: Sequelize.STRING
      },
      datacite_creator: {
        type: Sequelize.STRING
      },
      datacite_title: {
        type: Sequelize.STRING
      },
      datacite_resourcetype: {
        type: Sequelize.STRING
      },
      datacite_publisher: {
        type: Sequelize.STRING
      },
      datacite_publicationyear: {
        type: Sequelize.STRING
      },
      created_by: {
        type: Sequelize.JSON
      },
      job_id: {
        type: Sequelize.STRING
      },
      nonstandard_headers: {
        type: Sequelize.JSON
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Metadata');
  }
};
