'use strict';
module.exports = (sequelize, DataTypes) => {
  var Metadatum = sequelize.define('Metadatum', {
    ark: DataTypes.STRING,
    _profile: DataTypes.STRING,
    erc_who: DataTypes.STRING,
    erc_what: DataTypes.STRING,
    erc_when: DataTypes.STRING,
    dc_creator: DataTypes.STRING,
    dc_title: DataTypes.STRING,
    dc_type: DataTypes.STRING,
    dc_publisher: DataTypes.STRING,
    dc_date: DataTypes.STRING,
    _target: DataTypes.STRING,
    datacite_creator: DataTypes.STRING,
    datacite_title: DataTypes.STRING,
    datacite_resourcetype: DataTypes.STRING,
    datacite_publisher: DataTypes.STRING,
    datacite_publicationyear: DataTypes.STRING,
    created_by: DataTypes.JSON,
    job_id: DataTypes.STRING,
    nonstandard_headers: DataTypes.JSON
  }, {});
  Metadatum.associate = function(models) {
    // associations can be defined here
  };
  return Metadatum;
};
