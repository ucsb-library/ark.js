.PHONY: test

test:
	VCR_MODE=playback npm test

html: lib
	node_modules/.bin/jsdoc lib -r -d html
