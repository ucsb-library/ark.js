# EzID ARK Minter

This is a web application for minting batches of ARKs.

Currently it

1. Accepts a spreadsheet (in any format [that SheetJS can
    parse](https://docs.sheetjs.com/#file-formats)) with object
    metadata described using the DataCite, DublinCore, or ERC
    [metadata
    profiles](http://ezid.cdlib.org/doc/apidoc.html#metadata-profiles);
1. Flags metadata that matches already-minted objects, and prompts the
   user to edit the existing record;
1. Updates and/or mints ARKs;
1. Allows the user to download an updated spreadsheet with the ARKs
   added;
1. Archives each user’s minting jobs so they can re-download them.

## Local development

1. Install dependencies: Node, SQLite, Redis
1. `npm i`
1. `npx sequelize db:migrate`
1. Start the server with `node index.js`

Run tests with `npm test`

### TODOs

- Allow batch edits within the web editor
- Allow comparison of local metadata for a record with CDL’s metadata
- Allow minting DOIs as well as ARKs

# Application Architecture/Components

## Framework

We use [Express](https://expressjs.com/) by itself without any MVC framework on
top.  Routes are specified in `index.js`, and the corresponding function for
each route is defined in `lib/handlers`.

## Database

The [Sequelize](http://docs.sequelizejs.com/) ORM allows us to use any database
that supports JSON, which we use for storing nonstandard headers.  By default we
use SQLite3 in development and PostgreSQL in production.

The database schema is defined in `db/models/metadatum.js`; migrations are run
with `node_modules/.bin/sequelize db:migrate`.

## Authentication

We use the [passport](http://passportjs.org/) library for
authentication.

By default the authentication method is
[LDAP](https://github.com/vesse/passport-ldapauth).

## Job queue

Interacting with EzID is done in the background. Jobs are queued and run using
[Bull](https://github.com/OptimalBits/bull), which requires Redis.  An admin UI
runs on port 5678 of the server; make sure it’s not open to the public!

# Configuration

Configuration of the application is done via environment variables.  For local
development, copy `.env.template` to `.env` and populate the variables.

| Name | Description | Required |
| ---- | ----------- | -------- |
| `DATABASE_URL` | Postgresql URL, including credentials | Yes, in production |
| `EZID_PASS` | Password for the [EzID](https://ezid.cdlib.org/doc/apidoc.html) account used for minting ARKs | Yes |
| `EZID_SHOULDER` | Shoulder for the EzID account used for minting ARKs | Yes |
| `EZID_USER` | Username for the EzID account used for minting ARKs | Yes |
| `LDAP_BASE` | Search base for the LDAP server used for authentication by [passport-ldapauth](https://github.com/vesse/passport-ldapauth#configure-strategy) | Yes |
| `LDAP_FILTER` | Search filter for the LDAP server; defaults to `(userprincipalname={{username}})` | Yes |
| `LDAP_PASS` | Password for the LDAP authenticating account | Yes |
| `LDAP_URL` | LDAP server URL | Yes |
| `LDAP_USER` | Username for the LDAP authenticating account | Yes |
| `SMTP_FROM` | Address that emails sent by the application should appear to be from | Yes |
| `SMTP_HOST` | Hostname of the SMTP server | Yes |
| `SMTP_PASS` | Password for authenticating to the SMTP server | No |
| `SMTP_PORT` | SMTP server port; default to 25 | Yes |
| `SMTP_TLS` | Whether to connect to the SMTP server securely; defaults to `false` | Yes |
| `SMTP_USER` | Username for authenticating to the SMTP server | No |
| `SUPERADMINS` | Comma-separated list of email addresses corresponding to users authorized to access the background jobs queue | No |

# Application flow

After logging in, a user is prompted to upload a spreadsheet with metadata for
objects they would like ARKs minted for.  The spreadsheet is first converted
into a JavaScript object by [SheetJS](https://github.com/sheetjs/js-xlsx).  We
then parse and compare it against previously minted objects, and generate a hash
holding the duplicate and new objects.

## In-memory data model

The hash model is roughly this:

There are two primary keys: `create` and `update`. The values of these
hashes are themselves also hashes, each with three keys, corresponding
to the three metadata profiles: `datacite`, `dc`, and `erc`.  The
values of these hashes are arrays, containing the records uploaded by
the user.  For example:

```js
{
  create: {
    erc: [
      {
        'erc.what': "A cat", 'erc.who': "Bastet", 'erc.when': "A long time ago"
      },
      {
        'erc.what': "Nothing", 'erc.who': "No one", 'erc.when': "Never"
      },
    ],
    dc: [
      {
        'dc.title': "Star War", 'dc.creator': "George Lukacs"
      },
    ],
    datacite: []
  },
  update: {
    erc: [
      {
        'erc.what': "An Everlasting Piece",
        'erc.who': "Billy Connolly",
        'erc.when': "2000",
        'ark': "ark:/99999/fk4f76kg6t"
      }
    ],
    dc: {},
    datacite: {}
  }
}
```

Objects that are flagged as duplicates are stored in `update`; records
are marked as duplicates if there’s a record in the database with the
same target/location URI or the same ARK.

The application will not mint a new ARK for these records, but the
user is given the opportunity to update the metadata of the existing
record, and the updated record is saved to the database.

Views use this hash to pre-fill forms with values.  If the user is
prompted to edit a subset of the objects they uploaded, the rest are
preserved using hidden `<input>` elements.

When the user submits the form, the hash is rebuilt using the new
parameters, and the next view is rendered.  When all existing records
have been updated and there are no more duplicates, the records in
`create` are minted.

## HTML table generation

The most fiddly part of the application is the generation of the HTML table
where the user can modify the metadata of the records they’re preparing to mink
ARKs for.  Because different fields should be rendered differently, we define a
`transform` function for each of the fields in `lib/render/tables/headers`.
A transform function has the following signature and is called by `entriesFor()`
in `lib/render/tables/generate.js`:

```js
/**
 * Generate an HTML table cell for this column header
 *
 * @param {string} status // either 'create' or 'update'
 * @param {string} profile // 'datacite', 'dc', or 'erc'
 * @param {number} index // the row index for the table
 * @param {string} cellValue // the actual value of the field we’re creating a cell for
 * @returns {string} HTML
 */
```

## Updating existing records

When the application mints a new ARK for an object, it stores the object
metadata in the database.  Every successive object provided by users is checked
against this database to ensure that we don’t mint a second ARK for the same
object.

Two different objects may have the same title (different versions of
the same book, two photographs by different artists, etc.), so all we
check is the target/location URI and the ARK.

If any objects are flagged as too similar upon upload, they are discarded.  The
_original_ object in the database that they were too similar to is placed in the
`update` hash, and the user is prompted to modify the metadata of that existing
record (to correct the typo, or whatever).  The
[`ezid`](https://www.npmjs.com/package/ezid) library is used to send the update
to EzID.  The database entry for the object is also updated, and the record is
removed from the `update` hash.

## Minting new ARKs

After the user has dealt with any duplicate objects, they are prompted to make
any final changes to the metadata of the new objects (those without ARKs).  The
following steps are then taken:

1. The new objects are given permanent ARK identifiers.
1. A new database entry is created for each original object, and database
   entries of existing records are updated.
1. The new objects are added to the user’s archive as a single batch job.  From
   there the user is able to download a spreadsheet of their objects, with the
   ARKs added.

These steps are performed in the background by [Bull](https://github.com/OptimalBits/bull).
