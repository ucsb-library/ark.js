require('dotenv').config()

const PORT = 3333

const Bull = require('bull')
const LdapStrategy = require('passport-ldapauth')
const bodyParser = require('body-parser')
const cookieSession = require('cookie-session')
const crypto = require('crypto')
const express = require('express')
const flash = require('connect-flash')
const helmet = require('helmet')
const jobs = require('./lib/jobs.js')
const passport = require('passport')
const util = require('util')

const { UI } = require('bull-board')
const { setQueues } = require('bull-board')

//
// Express and extensions
//
const app = express()

app.set('view engine', 'ejs')
app.use(express.static(`${__dirname}/public`))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(flash())

// authentication
app.use(helmet())
app.use(cookieSession({
  name: 'session',
  keys: [crypto.randomBytes(10).toString('base64')],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))
app.use(passport.initialize())
app.use(passport.session())
passport.serializeUser((user, callback) => {
  callback(null, user.userPrincipalName)
})
passport.deserializeUser((obj, callback) => {
  callback(null, obj)
})

const ldap_conf = require('./config/ldap.js')
passport.use(new LdapStrategy(ldap_conf))

function auth(req, res, next) {
  if (req.user) {
    return next()
  } else {
    return res.redirect('/')
  }
}

function superauth(req, res, next) {
  const superadmins = require('./config/superadmins.js')

  const isSuper = superadmins.some((value, index, array) => {
    return value === req.user
  })

  if (isSuper) {
    return next()
  } else {
    return res.redirect('/')
  }
}

//
// Route handling
//
const handle = require('./lib/handlers.js')
app.get('/', handle.root)

app.post(/^\/login\/?$/,
         passport.authenticate('ldapauth', {
           successRedirect: '/upload',
           failureRedirect: '/',
           failureFlash: true
         }))
app.get(/^\/logout\/?$/, (req, res) => {
  req.logout();
  res.redirect('/');
})

// file upload flow
app.get(/^\/upload\/?$/, auth, handle.uploadScreen)
app.post(/^\/upload\/?$/, auth, handle.uploadFile)

// Minting or updating ARKs
app.post(/^\/create\/?$/, auth, handle.create)
app.post(/^\/update\/?$/, auth, handle.update)

// viewing/downloading previous jobs
app.get(/^\/archive\/?$/, auth, handle.archive)
app.get(/^\/download\/?$/, auth, handle.download)

// job processing admin UI
app.use('/admin/queues', superauth, UI)

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})

//
// Job processing
//
const redisUrl = process.env.REDIS_URL || 'redis://127.0.0.1:6379'

const createJob = new Bull('create', redisUrl)
const updateJob = new Bull('update', redisUrl)
const emailJob = new Bull('email', redisUrl)

setQueues([
  createJob,
  updateJob,
  emailJob
])

createJob.process((job, done) => {
  jobs.create(job, done)
})
updateJob.process((job, done) => {
  jobs.update(job, done)
})
emailJob.process((job, done) => {
  jobs.mail(job, done)
})
