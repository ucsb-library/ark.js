FROM node:8-alpine

ENV NODE_ENV production

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  curl \
  git \
  less \
  postgresql-dev \
  tzdata

RUN npm update -g npm

USER node

RUN mkdir -p /home/node/app
WORKDIR /home/node/app
# https://github.com/mhart/alpine-node/issues/48#issuecomment-431049230
COPY --chown=node:node package* ./
RUN npm i
COPY --chown=node:node . ./

CMD ["bin/run.sh"]
